﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace ChartReact.Controllers.ApiController
{
    [Route("api/[controller]")]
    [ApiController]
    public class ApiController : Controller
    {
        [HttpGet("getjson")]
        public async Task<IActionResult> GetJson()
        {
            string json = JsonConvert.SerializeObject(new
            {
               
                    //stateBarInfo = new List<string>()
                    //{
                    //   { "65" },
                    //   { "50" },
                    //   { "55" },
                    //   { "40" },
                    //   { "35" },
                    //   { "30" }
                    //},
                    //stateHorizontalBarInfo = new List<string>()
                    //{
                    //   { "65" },
                    //   { "50" },
                    //   { "55" },
                    //   { "40" },
                    //   { "35" },
                    //   { "30" }
                    //},
                    //statePieInfo = new List<string>()
                    //{
                    //  { "65" },
                    //   { "50" },
                    //   { "55" },
                    //   { "40" },
                    //   { "35" },
                    //   { "30" }
                    //},
                    stateLineInfo = new List<string>()
                    {
                       { "65" },
                       { "50" },
                       { "55" },
                       { "40" },
                       { "35" },
                       { "30" }
                    }
            });

            return Ok(json);
        }
    }
}
